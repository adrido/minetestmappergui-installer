qmake.exe "%PROJDIR%\MinetestMapperGui.pro" -spec win32-msvc2015 CONFIG+=production

C:/Qt/Tools/QtCreator/bin/jom.exe /F Makefile.Release
C:/Qt/Tools/QtCreator/bin/jom.exe clean
windeployqt.exe .\release\MinetestMapperGui.exe --no-opengl-sw --no-angle
lrelease.exe "%PROJDIR%\MinetestMapperGui.pro"
xcopy /y "%PROJDIR%\translations\*.qm" .\release\translations\
xcopy /y "%PROJDIR%\translations\*.png" .\release\translations\
xcopy /y "%PROJDIR%\colors\*.txt" .\release\colors\
xcopy /y "%MAPPER_BIN_DIR%\*.exe" .\release\
xcopy /y "%MAPPER_BIN_DIR%\*.dll" .\release\
xcopy /y "..\MinetestMapperGui_Portable.bat" .\release\

if not exist "..\Output" mkdir "..\Output"
