@echo off
echo Setting up environment for Qt usage...
set PATH=C:\Qt\5.8\msvc2015_64\bin;%PATH%
set "PROJDIR=D:\Users\Doll\Documents\Qt-Projekte\minetestmappergui"
set "MAPPER_BIN_DIR=D:\Users\Doll\Documents\Projects\minetest-mapper-cpp\x64\Release"
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
if not exist ".\pack_x64" mkdir ".\pack_x64"
cd "pack_x64"

call "..\__build.bat"

"C:\Program Files\7-Zip\7z.exe" u "..\Output\MinetestMapperGui_64.zip" ".\release\"

cd ..\
echo Creating Setup
"C:\Program Files (x86)\Inno Setup 5\iscc.exe" "InnoSetupScript_x64.iss"
pause
