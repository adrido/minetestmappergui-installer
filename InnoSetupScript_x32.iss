

#define MyAppVersion GetFileVersion('pack_x32\release\MinetestMapperGui.exe')
#include "InnoSetupScript.iss"

[Setup]

OutputBaseFilename=Setup {#MyAppName} x32
MinVersion=5.01sp3

[Run]
Filename: "{tmp}\vcredist_x86.exe"; Parameters: "/install /passive /norestart"; Flags: 32bit; Description: "Microsoft Visual C++ 2015 Redistributable"; StatusMsg: "Installing Microsoft Visual C++ 2015 Redistributable"


[Files]
Source: "pack_x32\release\*"; Excludes: "vcredist_x86.exe"; DestDir: "{app}"; Flags: ignoreversion createallsubdirs recursesubdirs 32bit
Source: "pack_x32\release\vcredist_x86.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall 32bit

