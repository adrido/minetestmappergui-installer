@echo off
echo Setting up environment for Qt usage...
set PATH=C:\Qt\5.8\msvc2015\bin;%PATH%
set "PROJDIR=D:\Users\Doll\Documents\Qt-Projekte\minetestmappergui"
set "MAPPER_BIN_DIR=D:\Users\Doll\Documents\Projects\minetest-mapper-cpp\Release"
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x86
if not exist ".\pack_x32" mkdir ".\pack_x32"
cd "pack_x32"

call "..\__build.bat"


"C:\Program Files\7-Zip\7z.exe" u "..\Output\MinetestMapperGui_32.zip" ".\release\"


cd ..\
"C:\Program Files (x86)\Inno Setup 5\iscc.exe" "InnoSetupScript_x32.iss"
pause