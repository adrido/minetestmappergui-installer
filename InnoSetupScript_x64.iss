

#define MyAppVersion GetFileVersion('pack_x64\release\MinetestMapperGui.exe')
#include "InnoSetupScript.iss"

[Setup]

OutputBaseFilename=Setup {#MyAppName} x64
MinVersion=0,6.0

[Run]
Filename: "{tmp}\vcredist_x64.exe"; Parameters: "/install /passive /norestart"; Flags: 64bit; Description: "Microsoft Visual C++ 2015 Redistributable"; StatusMsg: "Installing Microsoft Visual C++ 2015 Redistributable"


[Files]
Source: "pack_x64\release\*"; Excludes: "vcredist_x64.exe"; DestDir: "{app}"; Flags: ignoreversion createallsubdirs recursesubdirs 64bit
Source: "pack_x64\release\vcredist_x64.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall 64bit

